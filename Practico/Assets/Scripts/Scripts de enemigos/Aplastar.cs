using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Aplastar : MonoBehaviour
{
    private bool bajar;
    public float velocidadCaida;
    public float velocidadSubida;

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            SceneManager.LoadScene(2);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Aplastando()
    {
        transform.position -= transform.up * velocidadCaida * Time.deltaTime;
    }

    void recargar()
    {
        transform.position += transform.up * velocidadSubida * Time.deltaTime;
    }


    // Update is called once per frame
    void Update()
    {
        if (transform.position.y >= 10)
        {
            bajar = true;
        }
        if (transform.position.y <= 0)

        {
            bajar = false;
        }

        if (bajar)
        {
            Aplastando();
        }
        else
        {
            recargar();
        }
    }
}
