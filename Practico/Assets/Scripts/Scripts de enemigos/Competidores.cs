using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Competidores : MonoBehaviour, IEnemigo
{
    #region Datos
    [SerializeField] private float vida;
    [SerializeField] private float rapidez;
    [SerializeField] private float da�oC;
    [SerializeField] private float Rango;
    bool avistamiento;
    public LayerMask player;
    public Transform jugador;
    #endregion
    public void Destruir()
    {
        Destroy(gameObject);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bala"))
        {
            if (vida <= 0)
            {
                Destruir();
            }
        }
    }

    private void OnCollisionEnter(Collision col)
    {
        if (col.collider.CompareTag("Player"))
        {
            col.collider.GetComponent<ControlPersonaje>().vida -= da�oC;
            Destroy(gameObject);
        }
    }

    public void recibirDa�o(float da�o)
    {
        vida -= da�o;
        if (vida <= 0)
        {
            Destruir();
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        avistamiento = Physics.CheckSphere(transform.position, Rango, player);

        if (avistamiento == true)
        {

            transform.LookAt(new Vector3(jugador.position.x, transform.position.y, jugador.position.z));
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(jugador.position.x, transform.position.y, jugador.position.z), rapidez * Time.deltaTime);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, Rango);
    }
}
