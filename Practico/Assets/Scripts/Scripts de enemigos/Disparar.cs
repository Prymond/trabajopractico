using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparar : MonoBehaviour
{
    public float velocidadB;
    public float tiempoEspera;
    public float eDa�o;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, tiempoEspera);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * velocidadB);
    }

    private void OnTriggerEnter(Collider other)
    {        
        if (other.CompareTag("Muro"))
        {
            Destroy(gameObject);
        }
        if (other.CompareTag("Player"))
        {
            Destroy(gameObject);
        }

    }

}
