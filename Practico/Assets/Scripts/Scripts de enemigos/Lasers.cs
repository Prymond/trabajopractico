using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lasers : MonoBehaviour
{
    [SerializeField] private float rapidezL;
    [SerializeField] private float da�oL;
    [SerializeField] private float RangoL;
    bool avistamientoL;
    public LayerMask player;
    public Transform jugador;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        avistamientoL = Physics.CheckSphere(transform.position, RangoL, player);

        if (transform.position.y >= 2)
            {
                avistamientoL = false;
            }
        
        if (transform.position.y <= -1.2f)
            {
            avistamientoL = true;
            }

        if (avistamientoL) 
        {
            ascender();
        }
        else
        {
            descender();
        }
    }

    void descender()
    {
        transform.position -= transform.up * rapidezL * Time.deltaTime;
    }

    void ascender()
    {
        transform.position += transform.up * rapidezL * Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            collision.collider.GetComponent<ControlPersonaje>().vida -= da�oL;

        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, RangoL);
    }
}
