using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Baldosas : MonoBehaviour
{
    [SerializeField]private float tiempoEspera;
    private Rigidbody rb;
    private bool caer = false;

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            StartCoroutine(Caida(collision));
        }
        if (collision.gameObject.CompareTag("Caida"))
        {
            Destroy(gameObject);
        }
    }

    /*public void OnTriggerEnter(Collider other)
    {

    }*/

    private IEnumerator Caida(Collision collision)
    {
        yield return new WaitForSeconds(tiempoEspera);
        caer = true;
        Physics.IgnoreCollision(transform.GetComponent<Collider>(), collision.transform.GetComponent<Collider>());
        rb.constraints = RigidbodyConstraints.None;
        rb.AddForce(new Vector3(5, 1, 1));
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
