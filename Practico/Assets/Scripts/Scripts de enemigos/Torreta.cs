using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torreta : MonoBehaviour, IEnemigo
{
    public Transform objetivo;
    public GameObject bala;
    bool avistamiento;
    public LayerMask player;

    public float tiempoCarga;
    [SerializeField] private float Rango;
    private float conteo = 0f;

    private int contador;
    public int contadorMax;

    IEnumerator Disparar()
    {
        for (int i = 0; i < contadorMax; i++)
        {
            contador++;
            Instantiate(bala, transform.position, transform.rotation);
            yield return new WaitForSeconds(tiempoCarga);
        }
    }

    private void Start()
    {

    }

    void Update()
    {

        avistamiento = Physics.CheckSphere(transform.position, Rango, player);

        if (avistamiento == true)
        {
            Vector3 Apuntar = objetivo.position - transform.position;
            Debug.DrawRay(transform.position, Apuntar, Color.red);

            Quaternion seguir = Quaternion.LookRotation(Apuntar);
            transform.rotation = Quaternion.Slerp(transform.rotation, seguir, Time.deltaTime);

            StartCoroutine(Disparar());
        }
    }

     

    [SerializeField] private float vida;
    public void Destruir()
    {
        Destroy(gameObject);
    }

    public void recibirDa�o(float da�o)
    {
        vida -= da�o;
        if (vida <= 0)
        {
            Destruir();
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, Rango);
    }
}
