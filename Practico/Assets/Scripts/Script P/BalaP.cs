using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaP : MonoBehaviour
{
    public float desaparecer = 1.5f;
    public float da�oB;


    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, desaparecer);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider collision)
    {
        IEnemigo da�ar = collision.GetComponent<IEnemigo>();
        if (collision.CompareTag("Enemy"))
        {
            da�ar.recibirDa�o(da�oB);
            Destroy(gameObject);
        }
        if (collision.CompareTag("Muro"))
        {
            Destroy(gameObject);
        }
    }
}
