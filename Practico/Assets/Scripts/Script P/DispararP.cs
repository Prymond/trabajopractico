using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DispararP : MonoBehaviour
{
    public Transform salida;
    public float velocidadD;
    public float tiempoBala;
    public float cadencia;
    public float cadenciaT = 0;
    public GameObject balaP;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Time.time > cadenciaT)
            {
                GameObject BalaP;
                BalaP = Instantiate(balaP, salida.position, salida.rotation);
                BalaP.GetComponent<Rigidbody>().AddForce(salida.forward * velocidadD);

                cadenciaT = Time.time + cadencia;
            }
        }
    }

}
