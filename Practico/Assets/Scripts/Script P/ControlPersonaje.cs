using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPersonaje : MonoBehaviour
{
    private Rigidbody rb;
    #region camara
    private Camera cam;
    private Camara cm;
    private Vector3 camFwd;
    #endregion

    #region Animacion
    private Animaciones animP;
    private bool bloquear = false;
    private bool Caminar = false;
    private bool GirarIzquierda = false;
    private bool GirarDerecha = false;
    private bool Retroceder = false;
    private bool Saltar = false;
    #endregion

    #region Datos
    [Range(1.0f, 10.0f)]
    public float VelocidadCaminar;
    [Range(1.0f, 10.0f)]
    public float VelocidadRetroceso;
    [Range(1.0f, 10.0f)]
    public float VelocidadGiro;
    [Range(0.1f, 10.0f)]
    public float VelocidadRotacion;
    [Range(1.0f, 10.0f)]
    public float FuerzaSalto;

    public float vida;
    #endregion


    private void Awake()
    {
        animP = FindObjectOfType<Animaciones>();
        cm = GetComponent<Camara>();
        cam = cm.ObtenerCam();
        rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        GestorSonido.instancia.ReproducirSonido("Ambiente");
    }

    void Update()
    {
        estadoMaquina();

        animP.GetFullBodyAnimator().SetBool("Caminar", Caminar);
        animP.GetFullBodyAnimator().SetBool("GirarDerecha", GirarDerecha);
        animP.GetFullBodyAnimator().SetBool("GirarIzquierda", GirarIzquierda);
        animP.GetFullBodyAnimator().SetBool("Retroceder", Retroceder);
        animP.GetFullBodyAnimator().SetBool("Saltar", Saltar);

    }

    void FixedUpdate()
    {
        if (!bloquear)
        {
            // Gets the input
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");
            Saltar = Input.GetButtonDown("Jump");

            // Calculate camera relative directions to move:
            camFwd = Vector3.Scale(cam.transform.forward, new Vector3(1, 1, 1)).normalized;
            Vector3 camFlatFwd = Vector3.Scale(cam.transform.forward, new Vector3(1, 0, 1)).normalized;
            Vector3 flatRight = new Vector3(cam.transform.right.x, 0, cam.transform.right.z);

            Vector3 m_CharForward = Vector3.Scale(camFlatFwd, new Vector3(1, 0, 1)).normalized;
            Vector3 m_CharRight = Vector3.Scale(flatRight, new Vector3(1, 0, 1)).normalized;


            // Draws a ray to show the direction the player is aiming at
            Debug.DrawLine(transform.position, transform.position + camFwd * 5f, Color.red);

            // Move the player (movement will be slightly different depending on the camera type)
            float VelCam;
            Vector3 move = Vector3.zero;
            if (cm.tipo == Camara.TipoCamara.Libre)
            {
                VelCam = VelocidadCaminar;
                move = vertical * m_CharForward * VelCam + horizontal * m_CharRight * VelocidadCaminar;
                cam.transform.position += move * Time.deltaTime;

                // Rotate body
                animP.transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(animP.transform.forward, move, VelocidadRotacion, 0.0f));
            }
            else if (cm.tipo == Camara.TipoCamara.Cerrado)
            {
                VelCam = (vertical > 0) ? VelocidadCaminar : VelocidadRetroceso;
                move = vertical * m_CharForward * VelCam + horizontal * m_CharRight * VelocidadGiro;
            }

            transform.position += move * Time.deltaTime;    // Move the actual player

            // Jump 
            if (Saltar)
            {
                rb.AddForce(Vector3.up * FuerzaSalto, ForceMode.Impulse);
            }

            // Update animation flags
            if (cm.tipo == Camara.TipoCamara.Libre)
            {
                Caminar = (horizontal != 0 || vertical != 0);

            }
            else if (cm.tipo == Camara.TipoCamara.Cerrado)
            {
                Caminar = (vertical > 0 && horizontal == 0);
                Retroceder = (vertical < 0 && horizontal == 0);
                GirarIzquierda = (horizontal < 0);
                GirarDerecha = (horizontal > 0);
            }
        }
    }

    void estadoMaquina() { } 

    public void bloquearMovimiento() { bloquear = true; }
    public void desbloquearMovimiento() { bloquear = false; }
    public Transform Tpersonaje() { return animP.transform; }



}
