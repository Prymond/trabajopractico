using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara : MonoBehaviour
{
    public Camera camara;
    #region
    public enum TipoCamara { Libre, Cerrado }
    private enum EstadoCamara { Mover, Bloquear}
    private EstadoCamara estado = EstadoCamara.Mover;

    public TipoCamara tipo = TipoCamara.Libre;

    [Range(0.1f, 2.0f)]
    public float sensibilidad;

    [Range(4.0f, 10.0f)]
    public float distancia;

    public bool invertirEjeX;
    public bool invertirEjeY;

    public Transform seguir;

    bool transicion;
    private estadoCamara comenzarEstado;
    private estadoCamara terminarEstado;
    private float tiempoTransicion = 0.0f;

    public Transform aimCam;
    #endregion
    private struct estadoCamara
    {
        public Vector3 posicion;
        public Vector3 rotacion;
        public Transform seguir;
        public float tiempo;
    }


    private void Awake()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        camara.transform.position = transform.position + distancia * new Vector3(0.65f, 4, -4).normalized;

        if (tipo == TipoCamara.Cerrado) { camara.transform.parent = transform; }
    }

    private void FixedUpdate()
    {
        if (!transicion)
        {
            if (estado == EstadoCamara.Mover)
            {
                float horizontal = Input.GetAxis("Mouse X");
                float vertical = Input.GetAxis("Mouse Y");

                horizontal = (invertirEjeX) ? (-horizontal) : horizontal;
                vertical = (invertirEjeY) ? (-vertical) : vertical;

                if (horizontal != 0)
                {
                    if (tipo == TipoCamara.Cerrado) transform.Rotate(Vector3.up, horizontal * 90 * sensibilidad * Time.deltaTime);
                    else if (tipo == TipoCamara.Libre) camara.transform.RotateAround(transform.position, transform.up, horizontal * 90 * sensibilidad * Time.deltaTime);
                }

                if (vertical != 0)
                {
                    camara.transform.RotateAround(transform.position, transform.right, vertical * 90 * sensibilidad * Time.deltaTime);
                }

                camara.transform.LookAt(seguir);

                Vector3 angulos = camara.transform.rotation.eulerAngles;
                camara.transform.rotation = Quaternion.Euler(new Vector3(angulos.x, angulos.y, 0));
            }
        }
        else
        {
            float t = (Time.time - comenzarEstado.tiempo) / (terminarEstado.tiempo - comenzarEstado.tiempo);
            camara.transform.position = Vector3.Lerp(comenzarEstado.posicion, terminarEstado.posicion, t);
            camara.transform.eulerAngles = Vector3.Lerp(comenzarEstado.rotacion, terminarEstado.rotacion, t);

            camara.transform.LookAt(terminarEstado.seguir);
            if (t > 1)
                transicion = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown("tab"))
            { Transicionar(aimCam.position, aimCam.rotation.eulerAngles, seguir, 1.5f); }
        if (Input.GetKeyDown("escape")) { Cursor.lockState = CursorLockMode.None; }

    }

    public Camera ObtenerCam() { return camara; }

    public void Transicionar(Vector3 PosFinal, Vector3 RotFinal, Transform seguirFinal, float duracion)
    {
        comenzarEstado.posicion = camara.transform.position;
        comenzarEstado.rotacion = camara.transform.rotation.eulerAngles;
        comenzarEstado.seguir = seguir;
        comenzarEstado.tiempo = Time.time;

        terminarEstado.posicion = PosFinal;
        terminarEstado.rotacion = RotFinal;
        terminarEstado.seguir = seguirFinal;
        terminarEstado.tiempo = comenzarEstado.tiempo + duracion;

        tiempoTransicion = duracion;
        transicion = true;
    }

    public void bloquearCamara() { estado = EstadoCamara.Bloquear; }
    public void liberarCamara() { estado = EstadoCamara.Mover; }
    
    public void volver()
    {
        Transicionar(transform.position + distancia * new Vector3(0.65f, 4, -4).normalized, Vector3.zero, seguir, 1.0f);
        liberarCamara();

    }
    public void Seguir(Transform la) { seguir = la; }
}
